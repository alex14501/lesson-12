#include "MessageSender.h"
#include <string>
#include <iostream>
#include <thread>
#define SIGN_IN 1
#define SIGN_OUT 2
#define CONNECTED_USERS 3
#define EXIT 4
int main()
{
	MessageSender* MessageCenter = new MessageSender();
	int choice = 0;
	std::string name;
	std::thread t1(&MessageSender::readMessages,MessageCenter);
	std::thread t2(&MessageSender::sendMessages, MessageCenter);
	t1.detach();
	t2.detach();
	std::cout << "Welcome to my Message Center!!!" << std::endl;
	while (choice != EXIT)
	{
		std::cout << "Please enter a Choice\n1.Sign In\n2.Sign Out\n3.Connected Users\n4.Exit" << std::endl;
		std::cin >> choice;
		switch (choice)
		{
		case SIGN_IN:
			std::cout << "Please enter you'r Username: ";
			std::cin >> name;
			MessageCenter->signIn(name);
			break;
		case SIGN_OUT:
			std::cout << "Please enter you'r Username: ";
			std::cin >> name;
			MessageCenter->signOut(name);
			break;
		case CONNECTED_USERS:
			MessageCenter->printConnectedUsers();
			break;
		default:
			break;
		}
	}
	system("pause");
	return 0;
}