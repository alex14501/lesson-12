#include <set>
#include <iostream>
#include <string>
#include "MessageSender.h"
#include <queue>
#include <thread>
#include <mutex>
#include <fstream>
std::mutex usersMutex;
std::mutex Messages;
std::condition_variable sendThreadMessages;
std::mutex cond;
MessageSender::MessageSender()
{
}
MessageSender::~MessageSender()
{

}
void MessageSender::signIn(std::string name)
{
	usersMutex.lock();
	if (!this->_checkExistingUser(name))
	{
		this->_Users.insert(name);
	}
	else
	{
		std::cout << "This User is already connected" << std::endl;
	}
	usersMutex.unlock();
}
void MessageSender::signOut(std::string name)
{
	usersMutex.lock();
	if (this->_checkExistingUser(name))
	{
		this->_Users.erase(name);
	}
	else
	{
		std::cout << "This User is not connected..." << std::endl;
	}
	usersMutex.unlock();
}
bool MessageSender::_checkExistingUser(std::string name)
{
	std::set<std::string>::iterator iter;
	iter=this->_Users.find(name);
	if (iter != this->_Users.end())
	{
		return true;
	}
	else
	{
		return false;
	}
	
}
void MessageSender::printConnectedUsers()
{
	usersMutex.lock();
	std::set<std::string>::iterator iter;
	for (iter = this->_Users.begin(); iter != this->_Users.end(); iter++)
	{
		std::cout << (*iter) << ": Connected!" << std::endl;
	}
	usersMutex.unlock();
}
void MessageSender::readMessages()
{
	std::unique_lock<std::mutex> lck(cond);
	
	std::string Message;
	std::string tempMessage;
	std::ifstream file;
	std::ofstream ofs;
	cond.unlock();
	while (true)
	{
		file.open("data.txt");
		int i = 0;
		Messages.lock();
		file >> tempMessage;
		while (tempMessage[i] != 0)//getting messages a a new line
		{
			if (tempMessage[i] == '/n')
			{
				this->_Messages.push(Message);
				Message.clear();
			}
			else
			{
				Message += tempMessage[i];
			}
			i++;
		}
		tempMessage.clear();
		if (Message[0] != 0)//if thre is a last message.
		{
			this->_Messages.push(Message);
			Message.clear();
		}
		file.close();
		ofs.open("data.txt", std::ofstream::out | std::ofstream::trunc);//cleaning the file
		ofs.close();
		Messages.unlock();
		sendThreadMessages.notify_all();//the conditional variable doesnt work. idk why, but i did a workaround.
		std::this_thread::sleep_for(std::chrono::milliseconds(60000));
	}
}
void MessageSender::sendMessages()
{
	std::string Message;
	std::ofstream file;
	std::set<std::string>::iterator iter;
	std::unique_lock<std::mutex> lck(cond);
	bool faleg = false;
	sendThreadMessages.wait(lck);
	while (true)
	{
		Messages.lock();
		file.open("output.txt", std::ios::app);
		if (file)
		{
			usersMutex.lock();
			while (!this->_Messages.empty())//workaround the unworking conditional vriable
			{
				for (iter = this->_Users.begin(); iter != this->_Users.end(); iter++)
				{
					file << (*iter) << ": " << this->_Messages.front() << "\n";
					faleg = true;
				}
				if (faleg)
				{
					this->_Messages.pop();
					faleg = false;
				}
			}
			usersMutex.unlock();
			file.close();
		}
		Messages.unlock();
		sendThreadMessages.wait(lck);
	}
}