#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <set>
#include <queue>
class MessageSender
{
public:
	MessageSender();
	~MessageSender();
	void signIn(std::string name);
	void signOut(std::string name);

	void printConnectedUsers();
	void readMessages();
	void sendMessages();
private:
	bool _checkExistingUser(std::string name);
	std::set<std::string> _Users;
	std::queue<std::string> _Messages;
};
