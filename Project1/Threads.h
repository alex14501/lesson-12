#pragma once
#include <iostream>
#include <thread>
#include <stdio.h>
#include <vector>
#include <ctime>
#include <iostream>
#include <fstream>
void I_LoveThreads();
void call_I_Love_Threads();
void getPrimes(int begin, int end, std::vector<int> & primes);
void printVecor(std::vector<int> primes);
std::vector<int> callGetPrimes(int begin, int end);
void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimeMultipleThreads(int begin, int end, std::string filePath, int N);
