#include <iostream>
#include <thread>
#include <stdio.h>
#include <vector>
#include <ctime>
#include <iostream>
#include "Threads.h"
#include <fstream>
#include <mutex>
#include <string>
std::mutex mtx;
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	bool flag = true;
	int i = 0;
	for (int j = begin; j <= end; j++)
	{
		i = 2;
		flag = true;
		while (i <= j / 2 && flag)
		{
			if (j % i == 0)
			{
				flag = false;
			}
			i++;
		}
		if (flag)
		{
			mtx.lock();
			file << std::to_string(j) << "\n";
			mtx.unlock();
		}
	}
}
void callWritePrimeMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int start = 0;
	int end_thread = 0;
	std::ofstream myfile;
	myfile.open(filePath);
	std::vector<std::thread> vec;
	std::clock_t start_clock;
	double duration;
	int* numbers = new int[N * 2];
	start_clock = std::clock();
	for (int i = 0; i < N; i++)
	{
		start = begin + (((end - begin) / N)*i);//getting thread part to calculte
		end_thread = begin + (((end - begin) / N)*(i + 1));
		numbers[i] = start;// new fix
		numbers[i + 1] = end_thread;
		vec.emplace_back(writePrimesToFile, std::ref(numbers[i]), std::ref(numbers[i + 1]), std::ref(myfile));
	}
	for (auto& t : vec) {
		t.join();
	}
	duration = (std::clock() - start_clock) / (double)CLOCKS_PER_SEC;
	std::cout << "time it took to run all the thread " << duration << std::endl;
	myfile.close();
}